<?php

/**

 * PayPal Setting & API Credentials

 * Created by Raza Mehdi .

 */



return [

    'mode'    => env('PAYPAL_MODE', 'sandbox'),

    'sandbox' => [

        'username'    => env('PAYPAL_SANDBOX_API_USERNAME', 'sb-l0jn4456410_api1.business.example.com'),

        'password'    => env('PAYPAL_SANDBOX_API_PASSWORD', 'XJ67FX2239MPMBJV'),

        'secret'      => env('PAYPAL_SANDBOX_API_SECRET', 'AQgmPTx3ixjKPRrGYL5vJiLSSIwjA.G-w-CcOqJM41jRVJMSU69dptjW'),

        'certificate' => env('PAYPAL_SANDBOX_API_CERTIFICATE', ''),

        'app_id'      => 'APP-80W284485P519543T',

    ],

    'live' => [

        'username'    => env('PAYPAL_LIVE_API_USERNAME', ''),

        'password'    => env('PAYPAL_LIVE_API_PASSWORD', ''),

        'secret'      => env('PAYPAL_LIVE_API_SECRET', ''),

        'certificate' => env('PAYPAL_LIVE_API_CERTIFICATE', ''),

        'app_id'      => '',

    ],

    'payment_action' => 'Sale',

    'currency'       => env('PAYPAL_CURRENCY', 'CAD'),

    'billing_type'   => 'MerchantInitiatedBilling',

    'notify_url'     => '',

    'locale'         => '',

    'validate_ssl'   => false,

];

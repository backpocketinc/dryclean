<?php

namespace App\Http\Controllers;

use App\Cart;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OrderConfirmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:profile');
    }

    public function index()
    {
        $response = Cart::select('cart.id', 'cart.uniqueid', 'cart.product', 'cart.title', 'cart.quantity', 'cart.size', 'cart.cost', 'products.feature_image')
            ->join('products', 'cart.product', '=', 'products.id')->where('cart.uniqueid', Session::get('uniqueid'))->get();

        $user = UserProfile::find(Auth::user()->id);
        return view('order-confirm', compact('response', 'user'));
    }

    public function confirmed()
    {
        return view('order-confirmed');
    }
}

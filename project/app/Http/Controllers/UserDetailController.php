<?php

namespace App\Http\Controllers;

use App\Credit;
use App\Order;
use App\UserProfile;
use App\ProductFav;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:profile',['except' => 'checkout','cashondelivery']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = UserProfile::find(Auth::user()->id);
        $orders = Order::where('customerid', Auth::user()->id)->orderBy('id','desc')->get();
        $credits_details = Credit::where('user_id', Auth::user()->id)->orderBy('id','desc')
            ->where('status','=',1)
            ->get();
        return view('new_pages.user_dashboard',compact('user','orders','credits_details'));
    }

    //get user details
    public function accinfo()
    {
        $user = UserProfile::find(Auth::user()->id);
        return view('new_pages.user_account_address',compact('user'));
    }

    //udate user details
    public function updateDetails(Request $request, $id)
    {
        $user = UserProfile::findOrFail($id);
        $input = $request->all();
        $user->update($input);
        return redirect()->back()->with('message','Account Information Updated Successfully.');

    }
    //user password change
    public function passChange(Request $request, $id)
    {
        //return $request;
        $user = UserProfile::findOrFail($id);
        if ($request->oldpass){
            if (Hash::check($request->oldpass, $user->password)){

                if ($request->newpass == $request->renewpass){

                    $input['password'] = Hash::make($request->newpass);
                }else{
                    Session::flash('error', 'Confirm Password Does not match.');
                    return redirect()->back();
                }
            }else{
                Session::flash('error', 'Current Password Does not match');
                return redirect()->back();
            }
        }
        $user->update($input);
        return redirect()->back()->with('message','Account Password Updated Successfully.');

    }
    //Add fav to table
    public function addFav(Request $request)
    {
        //$user = UserProfile::find(Auth::user()->id);
       
        $user = UserProfile::find(Auth::user()->id);
        $product_fav = new ProductFav;
        $product_fav['user_id'] = $user->id;
        $product_fav['product_id'] = $request->product_id;
        $product_fav['status'] = $request->status;

        $product_fav->save();
       
        return response()->json(['msg' => "Sucessfully Added to Favourite"]);
        // $user = UserProfile::findOrFail($id);
        // $input = $request->all();
        // $user->update($input);
        // return redirect()->back()->with('message','Account Information Updated Successfully.');
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

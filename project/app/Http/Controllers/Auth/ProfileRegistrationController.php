<?php

namespace App\Http\Controllers\Auth;


use App\Category;
use App\Http\Controllers\Controller;
use App\Profile;
use App\UserProfile;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegistrationMail;
use App\Models\EmailSubject;
use App\Models\EmailTemplate;

class ProfileRegistrationController extends Controller
{

    protected $redirectTo = '/account';


    public function __construct()
    {
        $this->middleware('guest:profile');
    }


    public function showRegistrationForm()
    {
        return view('registeruser');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        //return $request;
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        if ($request->has('page') && $request->page == 'summary') {
            $this->guard()->login($user);

            return $this->registered($request, $user)
                ?: redirect(route('order.confirm'));
        }
        if ($request->has('page') && $request->page == 'register') {
            $this->guard()->login($user);
            return $this->registered($request, $user)
                //?: redirect(route('user-dashboard.index'));
                //?: redirect(route('order.confirm'));
                ?: redirect(url('/category/dry-clean-laundry'));
        }

        Session::flash('success', 'Registration Completed Successfully.');
        return redirect()->back()
            ->with('message', 'Registration Completed Successfully.');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('profile');
    }

    protected function registered(Request $request, $user)
    {
        //
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'name' => 'required|max:255',
            'first_name' => 'required|max:500',
            'last_name' => 'required|max:500',
            'email' => 'required|email|max:255|unique:user_profiles',
            'password' => 'required|min:6|confirmed',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $EmailTemplate = EmailTemplate::where('domain', 1)->where('subject_id', EmailSubject::where('subject', 'User Welcome - Customer')->first()['id'])->first();

        Mail::to($data['email'])->send(new UserRegistrationMail($data['first_name'], $EmailTemplate));
        $name = $data['first_name'] . " " . $data['last_name'];
        return UserProfile::create([
            'name' => $name,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'address' => isset($data['address']) ? $data['address'] : '',
            'zip' => isset($data['postal_code']) ? $data['postal_code'] : '',
            'city' => isset($data['locality']) ? $data['locality'] : '',
            'password' => Hash::make($data['password']),
        ]);
    }
}

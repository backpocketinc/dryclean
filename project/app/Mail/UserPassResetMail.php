<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserPassResetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $password;
    public $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($password, $template)
    {
        $this->password = $password;
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Password Reset Request - UBE DRY CLEAN')
            ->view('emails.registration.reset-password');
    }
}

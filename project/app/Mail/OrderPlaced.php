<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $template;
    public $button;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $template)
    {
        $this->order = $order;
        $this->template = $template;
        $this->button = "<a href='" . route('user.orders') . "' target='_blank'>MY ORDERS</a>";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Order Placed - UBE DRY CLEAN')
        ->view('emails.orders.placed');
    }
}

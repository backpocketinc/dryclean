<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistrationMail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $name;
    public $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $template)
    {
        $this->name = $name;
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome - UBE DRY CLEAN')
        ->view('emails.registration.registered');
    }
}

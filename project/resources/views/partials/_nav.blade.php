<!-- BEGIN SIDEBPANEL-->
        <nav class="page-sidebar" data-pages="sidebar">
            <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
            <!-- <div class="sidebar-overlay-slide from-top" id="appMenu">
          <div class="row">
            <div class="col-xs-6 no-padding">
              <a href="#" class="p-l-40"><img src="assets/img/demo/social_app.svg" alt="socail">
              </a>
            </div>
            <div class="col-xs-6 no-padding">
              <a href="#" class="p-l-10"><img src="assets/img/demo/email_app.svg" alt="socail">
              </a>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6 m-t-20 no-padding">
              <a href="#" class="p-l-40"><img src="assets/img/demo/calendar_app.svg" alt="socail">
              </a>
            </div>
            <div class="col-xs-6 m-t-20 no-padding">
              <a href="#" class="p-l-10"><img src="assets/img/demo/add_more.svg" alt="socail">
              </a>
            </div>
          </div>
        </div> -->
            <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
            <!-- BEGIN SIDEBAR MENU HEADER-->
            <div class="sidebar-header">
                <!-- <img src="assets/img/logo_white.png" alt="logo" class="brand" data-src="assets/img/logo_white.png"
            data-src-retina="assets/img/logo_white_2x.png" width="78" height="22"> -->
                <strong>LOGO</strong>
                <div class="sidebar-header-controls">
                    <!-- <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i
                class="fa fa-angle-down fs-16"></i>
            </button> -->
                    <button type="button"
                        class="btn btn-link d-lg-inline-block d-xlg-inline-block d-md-inline-block d-sm-none d-none"
                        data-toggle-pin="sidebar"><i class="fa fs-12"></i>
                    </button>
                </div>
            </div>
            <!-- END SIDEBAR MENU HEADER-->
            <!-- START SIDEBAR MENU -->
            <div class="sidebar-menu">
                <!-- BEGIN SIDEBAR MENU ITEMS-->
                <ul class="menu-items">
                    <li class="m-t-30" {!! (Request::is('user-dashboard.index') ? 'class="active"' : '') !!}>
                        <a href="{{route("user-dashboard.index")}}" class="detailed">
                            <span class="title">Dashboard</span>
                            {{-- <span class="details">12 New Updates</span> --}}
                        </a>
                        <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
                    </li>
                    <li {!! (Request::is('user/account-details') ? 'class="active"' : '') !!}>
                    <a href="{{route("user.account-details")}}" class="detailed">
                            <span class="title">My Account</span>
                        </a>
                        <span class="icon-thumbnail"><i class="pg-charts"></i></span>
                    </li>
                    <li {!! (Request::is('user-favourites') ? 'class="active"' : '') !!}>
                        <a href="{{route("user-favourites")}}" class="detailed">
                            <span class="">My Faves</span>
                        </a>
                        <span class="icon-thumbnail"><i class="pg-unordered_list"></i></span>
                    </li>
                    <li {!! (Request::is('user-billing-setting') ? 'class="active"' : '') !!}>
                        <a href="{{route("user-billing-setting")}}" class="detailed">
                            <span class="">Billing History</span>
                        </a>
                        <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    </li>
                    {{-- <li>
                        <a href="manage-envelopes.html" class="detailed">
                            <span class="">Manage Envelopes</span>
                        </a>
                        <span class="icon-thumbnail"><i class="fa fa-envelope"></i></span>
                    </li> --}}
                </ul>
    
                <div class="clearfix"></div>
            </div>
            <!-- END SIDEBAR MENU -->
        </nav>
        <!-- END SIDEBAR -->
        <!-- END SIDEBPANEL-->
        <!-- START PAGE-CONTAINER -->
        <div class="page-container ">
            <!-- START HEADER -->
            <div class="header ">
                <!-- START MOBILE SIDEBAR TOGGLE -->
                <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu" data-toggle="sidebar">
                </a>
                <!-- END MOBILE SIDEBAR TOGGLE -->
                <div class="">
                    <div class="brand inline   ">
                        <!-- <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png"
                data-src-retina="assets/img/logo_2x.png" width="78" height="22"> -->

                        <a href="{{url('/category')}}/dry-clean-laundry"><strong>Orders</strong></a>
                    </div>
                    <div class="brand inline   ">
                        <input type="text" style="border-color:#8533ff !important " class="form-control" placeholder="Search for any Receipt,by Vendor Name or Product">
                    </div>
    
                </div>
                <div class="d-flex align-items-center">
                    <!-- START User Info-->
                    <div class="pull-left p-r-10 fs-14 font-heading d-lg-block d-none">
                        <span class="semi-bold">{{ Auth::guard('profile')->user()->first_name }}</span> <span class="text-master">{{ Auth::guard('profile')->user()->last_name }}</span>
                    </div>
                    <div class="dropdown pull-right d-lg-block d-none">
                        <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <span class="thumbnail-wrapper d32 circular inline">
                                <img  src="{{ URL::asset('new_assets/assets/img/profiles/avatar.jpg')}}" alt="" data-src="{{ URL::asset('new_assets/assets/img/profiles/avatar.jpg')}}"
                                      data-src-retina="{{ URL::asset('new_assets/assets/img/profiles/avatar_small2x.jpg')}}" width="32" height="32">
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                            <a href="{{route("user.account-details")}}" class="dropdown-item"><i class="pg-settings_small"></i> Account
                                Settings</a>
                            <a href="#" class="dropdown-item"><i class="pg-outdent"></i> Feedback</a>
                            <a href="#" class="dropdown-item"><i class="pg-signals"></i> Help</a>

                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="clearfix bg-master-lighter dropdown-item">
                                <span class="pull-left">Logout</span>
                                <span class="pull-right"><i class="pg-power"></i></span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                    <!-- END User Info-->
                    <!-- <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block"
              data-toggle="quickview" data-toggle-element="#quickview"></a> -->
                </div>
            </div>
            <!-- END HEADER -->
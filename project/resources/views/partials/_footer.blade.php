<div class=" container-fluid  container-fixed-lg footer">
        <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
                &copy;2019 Backpocket Inc.</span><span class="hint-text"> All Rights Reserved</span>
            </p>
            <div class="clearfix"></div>
        </div>
</div>
@extends('includes.newmaster2',['cart_result'=> $response])
@section('content')

<style>
    #filter {
        text-align: center;
    }

    #sns_custommenu ul.mainnav li.level0>a>span.title {
        font-size: 13px;
        font-weight: 700;
        font-family: Poppins;
        position: relative;
        text-transform: uppercase;
        -webkit-transition: all .2s ease-out 0s;
        transition: all .2s ease-out 0s
    }

    #tableArea {
        -webkit-box-shadow: 0px 0px 5px 0px rgba(219, 219, 219, 1);
        -moz-box-shadow: 0px 0px 5px 0px rgba(219, 219, 219, 1);
        box-shadow: 0px 0px 5px 0px rgba(219, 219, 219, 1);
        padding: 0px 5px;
        margin-top: 20px;
    }

    #productTable,
    .bootstrap-select {
        font-family: 'Raleway', sans-serif;
        width: 100% !important;
    }

    .btn,
    input {
        border-radius: 0px !important;
    }

    #productTable {
        position: -webkit-sticky;
        /* Safari */
        position: sticky;
        top: 0;
    }


    #productTable .number {
        /* margin-left: 7px; */
        /* margin-right: 3px; */
        padding: 0;
        margin: 0px;
        font-size: 14px;
        border-style: none;
        background-color: transparent;
        width: 16px;
    }

    #productTable .icons i {
        top: 0;
        color: #652C91;
    }

    #productTable .icons i:first-child {
        padding-right: 7px;
    }

    #productTable .icons i:last-child {
        padding-left: auto;
    }

    .cart-icon {
        top: 0;
        font-size: 16px;
    }

    .home-wrapper .inner-block {
        /* width: 40%; */
        margin: 0 auto;
    }

    .home-wrapper .inner-block table thead {
        font-size: 18px;
    }

    .home-wrapper .inner-block table tbody {
        font-size: 16px;
        font-weight: 500;
    }

    .home-wrapper .inner-block table tbody .icons i {
        font-size: 15px;
    }

    label#searchProduct-error.error {
        color: #ff0000;
        font-size: 13px;
        padding-top: 5px;
        text-transform: uppercase;
    }

    #tableArea #totalTable {
        width: 70%;
        margin-left: auto;
        background-color: transparent;
        padding: 0;
    }

    #tableArea #totalTable td {
        border-top: 0;
        font-size: 13px;
        font-weight: 600;
        padding-bottom: 0;
    }

    h4.sec-title {
        font-size: 22px;
        font-weight: 600;
        margin-bottom: 20px;
    }

    .login-btn {
        background-color: #0059B2;
        color: #fff;
        font-weight: 600;
    }

    #emptyCart {
        padding: 20px;
        font-size: 14px;
        font-weight: 600;
    }

    #emptyCart i {
        font-size: 70px;
    }

    @media screen and (min-width : 1650px) {
        .home-wrapper .inner-block {
            width: 30%;
            margin: 0 auto;
        }
    }

    @media screen and (min-width : 2560px) {
        .home-wrapper .inner-block {
            width: 30%;
            margin: 0 auto;
        }
    }

    @media screen and (max-width : 1024px) {
        .home-wrapper .inner-block {
            width: 60%;
            margin: 0 auto;
        }
    }

    @media screen and (max-width : 480px) {
        .home-wrapper .container.inner-block {
            width: 100%;
            margin: 0;
            padding: 0;
        }

        .home-wrapper .inner-block table thead {
            font-size: 14px;
        }

        .home-wrapper .inner-block table tbody {
            font-size: 12px;
            font-weight: 500;
        }

        .home-wrapper .inner-block table tbody .icons i {
            font-size: 13px;
        }

        .home-wrapper .inner-block table tbody .add-cart i {
            font-size: 15px;
        }

        #productTable .number {
            /* margin-left: 8px; */
            /* margin-right: 2px; */
            /* width: 10px; */
        }
    }

    @media screen and (min-width: 1650px) {
        .home-wrapper .inner-block {
            width: 60% !important;
            margin: 0 auto;
        }
    }

    .warning-text {
        color: #ff0000;
        font-size: 14px;
        font-weight: 600;
        margin-top: 10px;
    }
</style>
@php
$price=0;
$items =0;
foreach($response as $res){
$price += $res->cost * $res->quantity;
$items += $res->quantity;

$user = Auth::user();
}
@endphp

@php
$quantities = 0;
$products = 0;
// $quantity = 0;
// $quantity = null;
@endphp
@foreach($response as $res)
@php
// $quantity += $res->quantity;
if ($products == 0 && $quantities == 0){
$products = $res->id;
$quantities = $res->quantity;
}else{
$products = $products.",".$res->id;
$quantities = $quantities.",".$res->quantity;
}
@endphp
@endforeach

@php
$delivery_fee=$settings[0]->delivery_fee;
@endphp
@php
$grandTotal =
number_format((float)($price+$delivery_fee) * 13/100 +
$price + $delivery_fee, 2, '.', '');
@endphp
<div class="home-wrapper">

    <div class="container-fluid">
        <!-- Starting of product filter area -->
        <div class="section-padding product-filter-wrapper wow fadeInUp">

            <div class="container inner-block">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-8">

                                <h4 class="sec-title">{{ $user->name }}</h4>
                                <table class="user-data" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 30%; border-top: 0; font-size: 13px;">Email <span
                                                style="float:right">:</span></td>
                                        <td style="border-top: 0; font-size: 13px;">{{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 0; font-size: 13px;">Phone <span
                                                style="float:right">:</span></td>
                                        <td style="border-top: 0; font-size: 13px;">{{ $user->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 0; font-size: 13px;">Address <span
                                                style="float:right">:</span></td>
                                        <td style="border-top: 0; font-size: 13px;">
                                            {{ $user->address }},<br>
                                            {{ $user->city }}, <br>
                                            {{ $user->zip }}
                                        </td>
                                    </tr>
                                </table>

                                <div id="googleMap" style="width:100%;height:250px;"></div>
                                <br>
                                <a href="{{ route('user.accinfo') }}" class="btn btn-sm btn-info">EDIT
                                    PROFILE</a>
                            </div>
                            <div class="col-md-4">

                                <h4 class="sec-title text-center">Your Account</h4>
                                <h5 class="balance text-center"><strong> Balance:
                                        ${{ number_format((float)$user->balance, 2, '.', '') }}
                                        Credits</strong></h5>
                                <div class="text-center">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#myModal">Load
                                        Credits</button>
                                </div>
                                <hr>

                                <div id="tableArea">
                                    @if($response->count() == 0)
                                    <div class="text-center" id="emptyCart">
                                        Hey! Looks like your cart is emapty, Please add some products! <br>
                                        <a href="{{ url('/category/dry-clean-laundry') }}"><i
                                                class="fas fa-cart-arrow-down"></i></a>
                                    </div>
                                    @else
                                    <table id="productTable" class="table table-striped tabele-bordered"
                                        style="margin-top:20px;">
                                        <thead>
                                            <tr style="text-transform: uppercase;">
                                                <th>Item</th>
                                                <th class="text-center">QTY</th>
                                                <th class="text-left">Rate</th>
                                                <th class="text-center">Total</th>
                                            </tr>
                                        </thead>

                                        <tbody style="font-size: 12px; font-weight: 500;">
                                            @foreach($response as $res)
                                            <tr>
                                                <td>
                                                    <a
                                                        href="{{ route('product.details', ['id' => $res->id, 'title' => str_slug(str_replace(' ', '-', $res->title))]) }}">{{ $res->title }}</a>
                                                </td>
                                                <td class="text-center">
                                                    {{ $res->quantity }}
                                                </td>
                                                <td class="text-left">
                                                    {{-- <img class="credit-sign"
                                                                            src="{{ url('/') . '/assets2/images/rsign.png' }}"
                                                    alt=""> --}}
                                                    ${{ number_format((float)$res->cost, 2, '.', '') }}
                                                </td>
                                                <td class="text-center">
                                                    {{-- <img class="credit-sign"
                                                                        src="{{ url('/') . '/assets2/images/rsign.png' }}"
                                                    alt=""> --}}
                                                    ${{ number_format((float)$res->cost * $res->quantity, 2, '.', '') }}
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="4">
                                                    <table id="totalTable">
                                                        <tr>
                                                            <td>
                                                                <span style="float:right">Subtotal:</span>
                                                            </td>
                                                            <td class="text-right">
                                                                ${{ number_format((float)$price, 2, '.', '') }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span style="float:right">Delivery:</span>
                                                            </td>
                                                            <td class="text-right">

                                                                ${{ number_format((float)$delivery_fee, 2, '.', '') }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span style="float:right">Tax (13%):</span>
                                                            </td>
                                                            <td class="text-right">
                                                                ${{ number_format((float) ($price+$delivery_fee) * 13/100, 2, '.', '') }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span style="float:right">Grand Total:</span>
                                                            </td>
                                                            <td class="text-right">

                                                                ${{ $grandTotal }}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            @endif
                                        </tbody>

                                    </table>
                                    {{-- @endif --}}
                                </div>
                                {{-- <br> --}}‌
                                <form action="{{ route('purchase.order') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="total" id="grandtotal"
                                        value="{{ number_format((float)($price+$delivery_fee) * 13/100 + $price + $delivery_fee, 2, '.', '') }}" />
                                    <input type="hidden" name="products" value="{{ $products }}" />
                                    <input type="hidden" name="quantities" value="{{ $quantities }}" />

                                    <input type="hidden" name="customer"
                                        value="{{Auth::guard('profile')->user()->id}}" />

                                    @if($response->count() !== 0)
                                    <div class="text-center">
                                        @if(($user->balance == 0) || ($user->balance < $grandTotal)) <button
                                            id="confirmBtn" class="btn btn-default" disabled>Confirm & Buy</button>
                                            @else
                                            <button type="submit" id="confirmBtn" class="btn btn-success">Confirm &
                                                Buy</button>
                                            @endif
                                            @if(($user->balance == 0) || ($user->balance < $grandTotal)) <p
                                                class="text-center warning-text">Please load your credits before you
                                                make
                                                the
                                                purchase!</p>
                                                @endif
                                    </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                section {
                    display: flex;
                    flex-flow: row wrap;
                }

                section>div {
                    flex: 1;
                    padding: 0.5rem;
                }

                input[type="radio"] {
                    display: none;

                    &:not(:disabled)~label {
                        cursor: pointer;
                    }

                    &:disabled~label {
                        color: hsla(150, 5%, 75%, 1);
                        border-color: hsla(150, 5%, 75%, 1);
                        box-shadow: none;
                        cursor: not-allowed;
                    }
                }

                label {
                    height: 100%;
                    display: block;
                    background: white;
                    border: 2px solid hsla(150, 75%, 50%, 1);
                    border-radius: 20px;
                    padding: 1rem;
                    margin-bottom: 1rem;
                    //margin: 1rem;
                    text-align: center;
                    box-shadow: 0px 3px 10px -2px hsla(150, 5%, 65%, 0.5);
                    position: relative;
                }

                input[type="radio"]:checked+label {
                    background: hsla(150, 75%, 50%, 1);
                    color: hsla(215, 0%, 100%, 1);
                    box-shadow: 0px 0px 20px hsla(150, 100%, 50%, 0.75);

                    &::after {
                        color: hsla(215, 5%, 25%, 1);
                        font-family: FontAwesome;
                        border: 2px solid hsla(150, 75%, 45%, 1);
                        content: "\f00c";
                        font-size: 24px;
                        position: absolute;
                        top: -25px;
                        left: 50%;
                        transform: translateX(-50%);
                        height: 50px;
                        width: 50px;
                        line-height: 50px;
                        text-align: center;
                        border-radius: 50%;
                        background: white;
                        box-shadow: 0px 2px 5px -2px hsla(0, 0%, 0%, 0.25);
                    }
                }

                input[type="radio"]#control_05:checked+label {
                    background: red;
                    border-color: red;
                }

                p {
                    font-weight: 900;
                }


                @media only screen and (max-width: 700px) {
                    section {
                        flex-direction: column;
                    }
                }
            </style>
            <!-- Trigger the modal with a button -->
            {{-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Buy Credits</button> --}}

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->

                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" style="font-size: 20px; font-weight: 700;">Buy
                                Credits</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-center">Select your desired credit package and click Buy Now to purchase,
                                Thanks!</p>
                            <br>
                            <section>
                                <div>
                                    <form action="{{ route('buy.credits') }}">
                                        {{ csrf_field() }}
                                        <input type="radio" id="control_01" name="select1" value="1">
                                        <input type="hidden" name="select" value="1">
                                        <label for="control_01">
                                            <h2>Starter</h2>
                                            <br>
                                            <p>Purchase <span style="font-size: 16px;">$30</span> package worth <span
                                                    style="font-size: 16px;">30 Credits</span></p>
                                            <button type="submit" class="btn btn-default"
                                                style="background: hsla(150, 75%, 50%, 1); color: #fff; font-weight: 500; margin-top: 10px;">Buy
                                                Now</button>
                                        </label>
                                    </form>
                                </div>
                                <div>
                                    <form action="{{ route('buy.credits') }}">
                                        {{ csrf_field() }}
                                        <input type="radio" id="control_02" name="select2" value="2" checked>
                                        <input type="hidden" name="select" value="2">
                                        <label for="control_02" style="background: hsla(150, 75%, 50%, 1); color:#fff;">
                                            <h2>Valued</h2>
                                            <br>
                                            <p>Purchase <span style="font-size: 16px;">$50</span> package worth <span
                                                    style="font-size: 16px;">60 Credits</span></p>
                                            <button type="submit" class="btn btn-default"
                                                style="background: #fff; color: hsla(150, 75%, 50%, 1); font-weight: 500; margin-top: 10px;">Buy
                                                Now</button>
                                        </label>
                                    </form>
                                </div>
                                <div>
                                    <form action="{{ route('buy.credits') }}">
                                        {{ csrf_field() }}
                                        <input type="radio" id="control_03" name="select3" value="3">
                                        <input type="hidden" name="select" value="3">
                                        <label for="control_03">
                                            <h2>Mega</h2>
                                            <br>
                                            <p>Purchase <span style="font-size: 16px;">$100</span> package worth <span
                                                    style="font-size: 16px;">125 Credits</span></p>
                                            <button type="submit" class="btn btn-default"
                                                style="background: hsla(150, 75%, 50%, 1); color: #fff; font-weight: 500; margin-top: 10px;">Buy
                                                Now</button>
                                        </label>
                                    </form>
                                </div>
                            </section>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Ending of product filter area -->
</div>


{{-- <form id="orderConfirm" action="#!" style="display:none">
    <input type="hidden" name="total" id="grandtotal"
        value="{{ number_format((float)($price+$delivery_fee) * 13/100 + $price + $delivery_fee, 2, '.', '') }}" />
<input type="hidden" name="products" value="{{ $products }}" />
<input type="hidden" name="quantities" value="{{ $quantities }}" />
<input type="hidden" name="sizes" value="{{$sizes}}" />
<input type="hidden" name="customer" value="{{Auth::guard('profile')->user()->id}}" />
{{ csrf_field() }}
</form> --}}

@stop

@section('footer')

<script>
    incrementVar = 1;
    function incrementValue(elem){
        var $this = $(elem);
            $input = $this.prev('input');
            $parent = $input.closest('div');
            newValue = parseInt($input.val())+1;
        $parent.find('.inc').addClass('a'+newValue);
        $input.val(newValue);      
        incrementVar += newValue;
    }
    function decrementValue(elem){
        var $this = $(elem);
            $input = $this.next('input');
            $parent = $input.closest('div');
            newValue = parseInt($input.val())-1;
        $parent.find('.inc').addClass('a'+newValue);
        if(newValue <= 1){
            $input.val(1);
        }else{
            $input.val(newValue);
        }
        incrementVar += newValue;
    }
</script>
<script>
    function myMap() {
var mapProp= {
  center:new google.maps.LatLng(51.508742,-0.120850),
  zoom:5,
};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRu_qlT0HNjPcs45NXXiOSMd3btAUduSc&libraries&callback=myMap">
</script>

<script>
    $(document).ready(function(e){
        // $("#confirmBtn").click(function(){
            // var balance = {!! $user->balance !!};
            // var total = {!! $grandTotal !!};
            
            // if(balance < total){
x
            // }
            // $formData = $("#orderConfirm").serialize();
            // $(this).prop('disabled', true);
            // $.ajax({
            //         url: "{!! route('purchase.order') !!}",
            //         data: $formData,
            //         type: 'POST',
            //         success: function(result){
            //             // console.log(result);
            //             if(result == "success"){
            //                 location.href = "{!! route('order.confirmed') !!}";
            //             }
            //             else{
            //                 alert('Something went wrong, please refresh the page, thank you!')
            //             }
                        
                        // console.log(result)
        //             }
        //         });
        // });
    });
</script>
@stop
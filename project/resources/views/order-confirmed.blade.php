@extends('includes.newmaster2',['cart_result'=> $response])
@section('content')

<div class="home-wrapper">

    <div class="container-fluid">
        <!-- Starting of product filter area -->
        <div class="section-padding product-filter-wrapper wow fadeInUp">

            <div class="container inner-block">
                <h3 class="text-center">Congrats!!!! Your order has been placed</h3>
                <p class="text-center"><b><a href="{{ url('user-dashboard') }}">CHECK MY ORDERS</a></b></p>
                <p class="text-center"><b>Thank you for shopping with us!</b></p>
            </div>
        </div>
    </div>
    <!-- Ending of product filter area -->
</div>

@stop

@section('footer')
@stop
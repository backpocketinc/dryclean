@extends('new_includes.new_main')

@section('title','| HomePage')



@section('content')
    <style>
        ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: red;
            opacity: 1; /* Firefox */
        }
        .top-right1 {
            position: absolute !important;
            /* top: 1px; */
            right: 0;
        }
        .font-clr{
            color: #B6B6B6;
        }
        .font-cl1{
            color: #8533ff;
        }

    </style>
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="#">Title</a></li> -->
                        <li class="">Dashboard</li>
                        <li style="color: #8533ff!important" class="top-right1">Account Balance CR$ 84.67</li>

                    </ol>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid">
            <!-- START card -->
            <div class="card card-default">
                <!-- <div class="card-header separator">
                     <div class="card-title">
                         <h5><strong>Transaction Details</strong></h5>

                     </div>
                 </div>-->
                <!-- <div class="card-body p-t-20">-->
                <!-- <div class="container-fluid"> -->
                <div class="row">
                    <div class="col-md-8">
                        <div class="card card-default">
                            <div class="invoice padding-20 sm-padding-10">
                                <!--<div>
                                    <div class="row">
                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-5"></div>
                                        <div class="col-md-3">
                                            <div class="sm-m-t-20">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>-->
                                <!--<div class="table-responsive table-invoice">
                                    <table class="table m-t-10">
                                        <thead>
                                        <tr>
                                            <th class="text-left">ITEM</th>
                                            <th class="text-center">QTY</th>
                                            <th class="text-right">AMOUNT</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="v-align-middle text-left">Product Name</td>
                                            <td class="v-align-middle text-center">1</td>
                                            <td class="v-align-middle text-right">$124.33</td>
                                        </tr>
                                        <tr>
                                            <td class="v-align-middle text-left">Product Name</td>
                                            <td class="v-align-middle text-center">1</td>
                                            <td class="v-align-middle text-right">$124.33</td>
                                        </tr>
                                        <tr>
                                            <td class="v-align-middle text-left">Product Name</td>
                                            <td class="v-align-middle text-center">1</td>
                                            <td class="v-align-middle text-right">$124.33</td>
                                        </tr>
                                        <tr>
                                            <td class="v-align-middle text-left">Product Name</td>
                                            <td class="v-align-middle text-center">1</td>
                                            <td class="v-align-middle text-right">$124.33</td>
                                        </tr>
                                        <tr>
                                            <td class="v-align-middle text-left">Product Name</td>
                                            <td class="v-align-middle text-center">1</td>
                                            <td class="v-align-middle text-right">$124.33</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>-->
                                <div class="card-body p-t-20">
                                    <form action="">
                                        <div class="row justify-content-left">
                                            <div class="col-md-4">
                                                <div class="form-group" style="display: inline-block">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h4><b>My Favourites</b></h4>
                                                            <p>Manage the List of items you see the most</p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="col-md-3">
                                                &lt;!&ndash; <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="reservation" id="daterangepicker"
                                                        class="form-control" value="08/01/2013 1:00 PM - 08/01/2013 1:30 PM">
                                                </div> &ndash;&gt;
                                               &lt;!&ndash; <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Quick Date</label>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <select class="form-control">
                                                            &lt;!&ndash; <option value="" selected disabled>Quick Date</option> &ndash;&gt;
                                                            <option value="">Today</option>
                                                            <option value="">This Week</option>
                                                            <option value="">This Month</option>
                                                            <option value="">This Year</option>
                                                        </select>
                                                    </div>
                                                </div>&ndash;&gt;
                                            </div>-->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input style="border-color:#8533ff !important " type="text" class="form-control" placeholder="SEARCH BY PRODUCT NAME">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group" style="display: inline-block">

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="font-cl1">SHOW</label>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <select style="border-color:#8533ff !important " class="form-control">
                                                                <option value="" selected disabled>25</option> ;
                                                                <option value="">25</option>
                                                                <option value="">50</option>
                                                                <option value="">75</option>
                                                                <option value="">100</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label class="font-cl1">ITEMS</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <hr>
                                    <div class="">
                                        <table class="table table-hover table-condensed table-responsive table-responsive"
                                               id="tableTransactions">
                                            <thead bgcolor="#1f217d">
                                            <tr>
                                                <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                                                Comman Practice Followed
                                                -->
                                                <th style="width:50%;"><font color="#fc7b03">ITEMS</font></th>
                                                <th style="width: 50%;"><font color="#fc7b03">Total Orders</font></th>
                                                <th style="width: 50%;"><font color="#fc7b03">Actions</font></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="v-align-middle">SHIRT</td>
                                                <td class="v-align-middle">11523</td>
                                                <td class="v-align-middle">
                                                    <div class="btn-group">
                                                        <a href="#!" class="btn btn-success">Remove
                                                        </a>

                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="v-align-middle">SHIRT</td>
                                                <td class="v-align-middle">11523</td>
                                                <td class="v-align-middle">
                                                    <div class="btn-group">
                                                        <a href="#!" class="btn btn-success">Remove
                                                        </a>

                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="v-align-middle">SHIRT</td>
                                                <td class="v-align-middle">11523</td>
                                                <td class="v-align-middle">
                                                    <div class="btn-group">
                                                        <a href="#!" class="btn btn-success">Remove
                                                        </a>

                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="v-align-middle">SHIRT</td>
                                                <td class="v-align-middle">11523</td>
                                                <td class="v-align-middle">
                                                    <div class="btn-group">
                                                        <a href="#!" class="btn btn-success">Remove
                                                        </a>

                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="v-align-middle">SHIRT</td>
                                                <td class="v-align-middle">11523</td>
                                                <td class="v-align-middle">
                                                    <div class="btn-group">
                                                        <a href="#!" class="btn btn-success">Remove
                                                        </a>

                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--<div class="row">
                                        <div class="col-md-2">
                                            <input type="checkbox" value="1" id="checkbox1" required name="terms">
                                            <label for="checkbox1" class="text-info small"> <a href="http://backpocket.ca/terms.html"
                                                                                               class="text-info ">SELECT ALL</a></label>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="checkbox" value="1" id="checkbox1" required name="terms">
                                            <label for="checkbox1" class="text-info small"> <a href="http://backpocket.ca/terms.html"
                                                                                               class="text-info ">
                                                DESELECT</a></label>
                                        </div>

                                    </div>-->

                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="background-color: #f0f0f0; padding: 40px">
                        <!--<div class="card card-default">
                            <div class="card-body">-->
                        <h5 class="font-weight-bold">Why Choose Favourite Items?</h5>
                        <p class="m-b-20">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>

                        <br>

                        <!--</div>
                    </div>-->
                    </div>
                </div>
                <!-- </div>-->
            </div>
            <!-- </div> -->
        </div>
        <!-- END card -->
    </div>
    <!-- END PAGE CONTENT -->
@endsection
@section('scripts')
    <!-- BEGIN VENDOR JS -->
    <script src="{{ URL::asset('new_assets/assets/plugins/pace/pace.min.js')}}"  type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/modernizr.custom.js')}}"  type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-ui/jquery-ui.min.js')}}"  type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/popper/umd/popper.min.js')}}"  type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/bootstrap/js/bootstrap.min.js')}}"  type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/select2/js/select2.full.min.js')}}" type="text/javascript" src=""></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/classie/classie.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/lib/d3.v3.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/nv.d3.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/utils.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/tooltip.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/interactiveLayer.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/models/axis.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/models/line.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/models/lineWithFocusChart.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/mapplic/js/hammer.min.js')}}"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/mapplic/js/jquery.mousewheel.js')}}"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/mapplic/js/mapplic.js')}}"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/rickshaw/rickshaw.min.js')}}"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-metrojs/MetroJs.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/skycons/skycons.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('new_assets/assets/plugins/datatables-responsive/js/datatables.responsive.js')}}" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{ URL::asset('new_assets/pages/js/pages.js')}}"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="{{ URL::asset('new_assets/assets/js/scripts.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <!-- <script src="assets/js/dashboard.js" type="text/javascript"></script> -->
    <script src="{{ URL::asset('new_assets/assets/js/scripts.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function (e) {
            var table = $('#tableTransactions');
            table.dataTable({
                "sDom": "<t><'row'<p i>>",
                "destroy": true,
                "scrollCollapse": true,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ ",
                    "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
                },
                "iDisplayLength": 5
            })


            //Date Pickers
            $('#daterangepicker').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <!-- END PAGE LEVEL JS -->
@endsection 
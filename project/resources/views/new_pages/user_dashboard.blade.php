@extends('new_includes.new_main')

@section('title','| User Dashboard')


    
@section('content')
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
                <div class="inner breadcrumb">
                    <!-- START BREADCRUMB -->
                    <!--<p>Dashboard</p>&nbsp

                    <p align="center">Dashboard</p>-->
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="#">Title</a></li> -->
                        <li class="">Dashboard</li>
                        <li style="color: #8533ff!important" class="top-right1">Account Balance CR{{$settings[0]->currency_sign}}{{ number_format($user->balance, 2) }}</li>

                    </ol>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid p-b-50">
            <!--<div class="row">
                <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                    <div class="row">
                        <div class="col-md-12 m-b-10">
                            &lt;!&ndash; START WIDGET D3 widget_graphTileFlat&ndash;&gt;
                            <div id="credits"
                                class="widget-8 card no-border bg-primary no-margin widget-loader-bar">
                                <div class="container-xs-height full-height">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat fs-11 all-caps text-white font-weight-bold">Account
                                                        Balance
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li>
                                                            <a data-toggle="refresh"
                                                                class="card-refresh text-black" href="#"><i
                                                                    class="card-icon card-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height ">
                                        <div class="col-xs-height col-top">
                                            <div class="row p-l-20 p-r-20">
                                                <div class="col-sm-6" style="width: 50%;">
                                                    <h4 class="p-b-5 m-b-5 text-white text-center">
                                                        $12,500
                                                        <br>
                                                        <small class="fs-10 all-caps">Credits</small>
                                                    </h4>
                                                </div>
                                                <div class="col-sm-6" style="width: 50%;">
                                                    <h4 class="p-b-5 m-b-5 text-white text-center">
                                                        $14,000
                                                        <br><small class="fs-10 all-caps m-b-5">cad$</small>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            &lt;!&ndash; END WIDGET &ndash;&gt;
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                    <div class="row">
                        <div class="col-md-12 m-b-10">
                            &lt;!&ndash; START WIDGET D3 widget_graphTileFlat&ndash;&gt;
                            <div class="widget-8 card no-border bg-success no-margin widget-loader-bar">
                                <div class="container-xs-height full-height">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat fs-11 all-caps text-white font-weight-bold">Weekly
                                                        Spending <i class="fa fa-chevron-right"></i>
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li>
                                                            <a data-toggle="refresh"
                                                                class="card-refresh text-black" href="#"><i
                                                                    class="card-icon card-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height ">
                                        <div class="col-xs-height col-top relative">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="p-l-20">
                                                        <h3 class="p-b-5 m-b-5 text-white">$14,000</h3>
                                                        <p class="small m-t-5 p-t-5">
                                                            <a href="#!" class="fs-10 text-white">VIEW
                                                                DETAILS</a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                </div>
                                            </div>
                                            <div class="widget-8-chart line-chart" data-line-color="black"
                                                data-points="true" data-point-color="success"
                                                data-stroke-width="2">
                                                <svg>
                                                    <g class="nvd3 nv-wrap nv-lineChart"
                                                        transform="translate(-10,10)">
                                                        <g>
                                                            <rect width="380" height="103" style="opacity: 0;">
                                                            </rect>
                                                            <g class="nv-x nv-axis"></g>
                                                            <g class="nv-y nv-axis"></g>
                                                            <g class="nv-linesWrap">
                                                                <g class="nvd3 nv-wrap nv-line"
                                                                    transform="translate(0,0)">
                                                                    <defs>
                                                                        <clipPath id="nv-edge-clip-24382">
                                                                            <rect width="380" height="103">
                                                                            </rect>
                                                                        </clipPath>
                                                                    </defs>
                                                                    <g clip-path="">
                                                                        <g class="nv-groups">
                                                                            <g class="nv-group nv-series-0"
                                                                                style="stroke-opacity: 1; fill-opacity: 0.5; fill: rgb(0, 0, 0); stroke: rgb(0, 0, 0);">
                                                                                <path class="nv-line"
                                                                                    d="M0,103L63.33333333333334,75.53333333333333L126.66666666666669,34.33333333333334L190,27.46666666666667L253.33333333333337,0L316.6666666666667,13.733333333333334L380,68.66666666666667">
                                                                                </path>
                                                                            </g>
                                                                        </g>
                                                                        <g class="nv-scatterWrap" clip-path="">
                                                                            <g class="nvd3 nv-wrap nv-scatter nv-chart-24382"
                                                                                transform="translate(0,0)">
                                                                                <defs>
                                                                                    <clipPath
                                                                                        id="nv-edge-clip-24382">
                                                                                        <rect width="380"
                                                                                            height="103">
                                                                                        </rect>
                                                                                    </clipPath>
                                                                                </defs>
                                                                                <g clip-path="">
                                                                                    <g class="nv-groups">
                                                                                        <g class="nv-group nv-series-0"
                                                                                            style="stroke-opacity: 1; fill-opacity: 0.5; stroke: rgb(0, 0, 0); fill: rgb(0, 0, 0);">
                                                                                            <circle cx="0"
                                                                                                cy="103" r="3"
                                                                                                class="nv-point nv-point-0"
                                                                                                style="stroke-width: 2px;">
                                                                                            </circle>
                                                                                            <circle
                                                                                                cx="63.33333333333334"
                                                                                                cy="75.53333333333333"
                                                                                                r="3"
                                                                                                class="nv-point nv-point-1"
                                                                                                style="stroke-width: 2px;">
                                                                                            </circle>
                                                                                            <circle
                                                                                                cx="126.66666666666669"
                                                                                                cy="34.33333333333334"
                                                                                                r="3"
                                                                                                class="nv-point nv-point-2"
                                                                                                style="stroke-width: 2px;">
                                                                                            </circle>
                                                                                            <circle cx="190"
                                                                                                cy="27.46666666666667"
                                                                                                r="3"
                                                                                                class="nv-point nv-point-3"
                                                                                                style="stroke-width: 2px;">
                                                                                            </circle>
                                                                                            <circle
                                                                                                cx="253.33333333333337"
                                                                                                cy="0" r="3"
                                                                                                class="nv-point nv-point-4"
                                                                                                style="stroke-width: 2px;">
                                                                                            </circle>
                                                                                            <circle
                                                                                                cx="316.6666666666667"
                                                                                                cy="13.733333333333334"
                                                                                                r="3"
                                                                                                class="nv-point nv-point-5"
                                                                                                style="stroke-width: 2px;">
                                                                                            </circle>
                                                                                            <circle cx="380"
                                                                                                cy="68.66666666666667"
                                                                                                r="3"
                                                                                                class="nv-point nv-point-6"
                                                                                                style="stroke-width: 2px;">
                                                                                            </circle>
                                                                                        </g>
                                                                                    </g>
                                                                                    <g class="nv-point-paths">
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                            <g class="nv-legendWrap"></g>
                                                            <g class="nv-interactive"></g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            &lt;!&ndash; END WIDGET &ndash;&gt;
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                    <div class="row">
                        <div class="col-md-12 m-b-10">
                            &lt;!&ndash; START WIDGET D3 widget_graphTileFlat&ndash;&gt;
                            <div class="widget-8 card no-border bg-success no-margin widget-loader-bar"  style="background: url('assets/img/dashboard_tile-envelopes.jpg') no-repeat center;">
                                <div class="container-xs-height full-height" style="background-color: rgba(0, 0, 0, 0.6)">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat fs-11 all-caps text-white font-weight-bold">Envelopes
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li>
                                                            <a data-toggle="refresh"
                                                                class="card-refresh text-black" href="#"><i
                                                                    class="card-icon card-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height ">
                                        <div class="col-xs-height col-top relative">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="p-l-20">
                                                        &lt;!&ndash; <h3 class="p-b-5 m-b-5 text-white">$14,000</h3> &ndash;&gt;
                                                        <p class="small m-t-5 p-t-5">
                                                            <a href="#!" class="fs-10 text-white">VIEW
                                                                DETAILS</a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            &lt;!&ndash; END WIDGET &ndash;&gt;
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                    <div class="row">
                        <div class="col-md-12 m-b-10">
                            &lt;!&ndash; START WIDGET D3 widget_graphTileFlat&ndash;&gt;
                            <div class="widget-8 card no-border bg-success no-margin widget-loader-bar" style="background: url('assets/img/dashboard_tile-support.jpg') no-repeat center;">
                                <div class="container-xs-height full-height" style="background-color: rgba(0, 0, 0, 0.6)">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat fs-11 all-caps text-white font-weight-bold">Support
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li>
                                                            <a data-toggle="refresh"
                                                                class="card-refresh text-black" href="#"><i
                                                                    class="card-icon card-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height ">
                                        <div class="col-xs-height col-top relative">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="p-l-20">
                                                        &lt;!&ndash; <h3 class="p-b-5 m-b-5 text-white">$14,000</h3> &ndash;&gt;
                                                        <p class="small m-t-5 p-t-5">
                                                            <a href="#!" class="fs-10 text-white">VIEW
                                                                DETAILS</a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            &lt;!&ndash; END WIDGET &ndash;&gt;
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <div class="col-lg-12 col-xl-6 col-xlg-5 p-b-5" style="border-color: black !important">
                    <div
                        class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle full-height d-flex flex-column">
                        <div class="card-header  top-right">
                            <div class="card-controls">
                                <ul>
                                    <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                                                class="card-icon card-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="padding-25">
                            <div class="pull-left">
                                <h2 class="no-margin">My Orders</h2>
                                <p class="no-margin">Recent Transactions</p>
                            </div>
                            <!--<h3 class="pull-right semi-bold"><sup>
                                    <small class="semi-bold">$</small>
                                </sup> 102,967
                            </h3>-->
                            <div class="clearfix"></div>
                        </div>
                        <div class="auto-overflow widget-11-2-table">
                            <table class="table table-condensed table-hover " id="tableStore1">
                                <thead bgcolor="#1f217d">
                                    <tr class="text-center">
                                        <th class="all-caps"><font color="#fc7b03">Date</font></th>
                                        <th class="all-caps"><font color="#fc7b03">Order ID</font></th>
                                        <th class="all-caps"><font color="#fc7b03">Amount</font></th>
                                        <th class="all-caps"><font color="#fc7b03">Status</font></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($orders as $order)
                                    <tr class="text-center">
                                        <td class="fs-12">{{$order->booking_date}}</td>
                                        <td class="fs-12"><a href="{{url('user/order/')}}/{{$order->id}}">{{$order->id}}</a></td>

                                        <td class="fs-12">{{$settings[0]->currency_sign}}{{$order->pay_amount}}</td>
                                        <td class="fs-12">
                                            <button style="background-color:#e6b800 !important" class="btn btn-primary btn-cons m-b-10 btn-block"
                                                    type="button"><span
                                                        class="bold">{{$order->status}}</span>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                   {{-- <tr class="text-center">
                                        <td class="fs-12">05/17/2019</td>
                                        <td class="fs-12"><a href="#!">2113</a></td>
                                        <td class="fs-12"><a href="#!">$50.00.00</a></td>
                                        <td class="fs-12">
                                            <button style="background-color:#e6b800 !important" class="btn btn-primary btn-cons m-b-10 btn-block"
                                                    type="button"><span
                                                    class="bold">At Plant</span>
                                            </button>
                                        </td>
                                    </tr>--}}
                                </tbody>
                            </table>
                        </div>
                        <!--<div class="auto-overflow widget-11-2-table">
                            <table class="table table-condensed table-hover" id="tableStore2">
                                <thead bgcolor="#1f217d">
                                <tr class="text-center">
                                    <th style="width: 20%;" class="all-caps"><font color="#fc7b03">Date</font></th>
                                    <th style="width: 30%;" class="all-caps"><font color="#fc7b03">Ref</font></th>
                                    <th style="width: 22%;" class="all-caps"><font color="#fc7b03">Transaction</font></th>
                                    <th style="width: 30%;" class="all-caps"><font color="#fc7b03">Amount</font></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">DEPOSIT</td>
                                    <td class="fs-12">$50.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">DEPOSIT</td>
                                    <td class="fs-12">$50.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">DEPOSIT</td>
                                    <td class="fs-12">$50.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">DEPOSIT</td>
                                    <td class="fs-12">$50.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">DEPOSIT</td>
                                    <td class="fs-12">$50.00.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">DEPOSIT</td>
                                    <td class="fs-12">$50.00.00</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>-->
                        <div class="padding-25 mt-auto">
                            <p class="small pull-right">
                                <a href="#"><span>View Order History</span> <i
                                        class="fa fs-12 fa-arrow-circle-o-right text-success m-l-10"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--<div class="col-lg-5 col-xl-3 col-xlg-3">
                    <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle ">
                        <div class="card-header  top-right">
                            <div class="card-controls">
                                <ul>
                                    <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                                                class="card-icon card-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="padding-25">
                            <div class="pull-left">
                                <h2 class="no-margin">My Envelopes</h2>
                                &lt;!&ndash; <p class="no-margin">Recent Transactions</p> &ndash;&gt;
                            </div>
                            &lt;!&ndash; <h3 class="pull-right semi-bold"><sup>
                                    <small class="semi-bold">$</small>
                                </sup> 102,967
                            </h3> &ndash;&gt;
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-11-2-table">
                            <table class="table table-condensed table-hover" id="tableEnvelope">
                                <thead>
                                    <tr class="text-center">
                                        <th class="all-caps">Name</th>
                                        <th class="all-caps">items</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="text-center">
                                        <td class="fs-12"><a href="#!">Name 1</a></td>
                                        <td class="fs-12"><a href="#!">5</a></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td class="fs-12"><a href="#!">Name 2</a></td>
                                        <td class="fs-12"><a href="#!">5</a></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td class="fs-12"><a href="#!">Name 3</a></td>
                                        <td class="fs-12"><a href="#!">5</a></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td class="fs-12"><a href="#!">Name 4</a></td>
                                        <td class="fs-12"><a href="#!">5</a></td>
                                    </tr>
                                    <tr class="text-center">
                                        <td class="fs-12"><a href="#!">Name 5</a></td>
                                        <td class="fs-12"><a href="#!">5</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="padding-25 mt-auto">
                            <p class="small">
                                <a href="#!"><span>Go To Envelopes Manager</span> <i
                                        class="fa fs-12 fa-arrow-circle-o-right text-success m-l-10"></i></a>
                            </p>
                        </div>
                    </div>
                </div>-->
                <div class="col-lg-12 col-xl-6 col-xlg-5 p-b-5">
                    <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle ">
                        <div class="card-header  top-right">
                            <div class="card-controls">
                                <ul>
                                    <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                                                class="card-icon card-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="padding-25">
                            <div class="pull-left">
                                <h2 class="no-margin">Account Balance</h2>
                                 <p class="no-margin">Recent Transactions</p>
                            </div>
                            <button style="height: 50px; background-color:#8533ff !important" type="button" class="btn btn-complete pull-right">
                            <font style="font-size: 16px"><b>{{$settings[0]->currency_sign}} {{ number_format($user->balance, 2) }}</b></font> <br><p style="font-size: 8px">CREDITS</p></button>
                             <!--<h3 class="pull-right semi-bold"><sup>
                                    <small class="semi-bold">$</small>
                                </sup> 102,967
                            </h3>-->
                            <div class="clearfix"></div>
                        </div>
                        <div class="auto-overflow widget-11-2-table">
                            <table class="table table-condensed table-hover" id="tableStore">
                                <thead bgcolor="#1f217d">
                                    <tr class="text-center">
                                        <th style="width: 20%;" class="all-caps"><font color="#fc7b03">Date</font></th>
                                        <th style="width: 30%;" class="all-caps"><font color="#fc7b03">Ref</font></th>
                                        <th style="width: 22%;" class="all-caps"><font color="#fc7b03">Transaction</font></th>
                                        <th style="width: 30%;" class="all-caps"><font color="#fc7b03">Amount</font></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{--<tr class="text-center">
                                        <td class="fs-12">05/17/2019</td>
                                        <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                        <td class="fs-12">DEPOSIT</td>
                                        <td class="fs-12">$50.00</td>--}}
                                    </tr>
                                    @foreach($credits_details as $credits_detail)
                                        <tr class="text-center">
                                            <td class="fs-12">{{$credits_detail->created_at}}</td>
                                            <td class="fs-12">{{$credits_detail->invoice_id}}</td>
                                            {{--<td class="fs-12"><a href="{{url('user/order/')}}/{{$order->id}}">{{$order->id}}</a></td>--}}
                                            <td class="fs-12">DEPOSIT</td>
                                            <td class="fs-12">{{$settings[0]->currency_sign}}{{ number_format($credits_detail->amount, 2) }}</td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="padding-25 mt-auto">
                            <p class="small pull-right">
                                <a href="#!"><span>View Billing History</span> <i
                                        class="fa fs-12 fa-arrow-circle-o-right text-success m-l-10"></i></a>
                                <button style="background-color:#33ccff !important" class="btn btn-primary"
                                        type="button"><span
                                        class="bold">Add Credits</span>
                                </button>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid p-b-50">
            <div class="row">
                <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                    <div class="row">
                        <div class="col-md-12 m-b-10">
                            <!-- START WIDGET D3 widget_graphTileFlat-->
                            <div class="widget-8 card no-border bg-success no-margin widget-loader-bar"  style="background: url('https://via.placeholder.com/650x533/O https://placeholder.com/?fbclid=IwAR0Fi46IlJgmHkMzPBeJkWEy70KQcIM-ERID6-b1KgWrz52gTmtSEiY3JlY') no-repeat center;">
                                <div class="container-xs-height full-height" style="background-color: rgba(0, 0, 0, 0.6)">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                            class="font-montserrat fs-11 all-caps text-white font-weight-bold">Envelopes
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li>
                                                            <a data-toggle="refresh"
                                                               class="card-refresh text-black" href="#"><i
                                                                    class="card-icon card-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height ">
                                        <div class="col-xs-height col-top relative">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="p-l-20">
                                                        <!-- <h3 class="p-b-5 m-b-5 text-white">$14,000</h3> -->
                                                        <p class="small m-t-5 p-t-5">
                                                            <a href="#!" class="fs-10 text-white">VIEW
                                                                DETAILS</a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                    <div class="row">
                        <div class="col-md-12 m-b-10">
                            <!-- START WIDGET D3 widget_graphTileFlat-->
                            <div class="widget-8 card no-border bg-success no-margin widget-loader-bar" style="background: url('new_assets/assets/img/dashboard_tile-support.jpg') no-repeat center;">
                                <div class="container-xs-height full-height" style="background-color: rgba(0, 0, 0, 0.6)">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                            class="font-montserrat fs-11 all-caps text-white font-weight-bold">Support
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li>
                                                            <a data-toggle="refresh"
                                                               class="card-refresh text-black" href="#"><i
                                                                    class="card-icon card-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height ">
                                        <div class="col-xs-height col-top relative">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="p-l-20">
                                                        <!-- <h3 class="p-b-5 m-b-5 text-white">$14,000</h3> -->
                                                        <p class="small m-t-5 p-t-5">
                                                            <a href="#!" class="fs-10 text-white">VIEW
                                                                DETAILS</a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                    <div class="row">
                        <div class="col-md-12 m-b-10">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- END CONTAINER FLUID -->

    </div>
    <!-- END PAGE CONTENT -->
@endsection 
@section('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ URL::asset('new_assets/assets/plugins/pace/pace.min.js')}}"  type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/modernizr.custom.js')}}"  type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-ui/jquery-ui.min.js')}}"  type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/popper/umd/popper.min.js')}}"  type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/bootstrap/js/bootstrap.min.js')}}"  type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/select2/js/select2.full.min.js')}}" type="text/javascript" src=""></script>
<script src="{{ URL::asset('new_assets/assets/plugins/classie/classie.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/lib/d3.v3.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/nv.d3.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/utils.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/tooltip.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/interactiveLayer.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/models/axis.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/models/line.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/nvd3/src/models/lineWithFocusChart.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/mapplic/js/hammer.min.js')}}"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/mapplic/js/jquery.mousewheel.js')}}"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/mapplic/js/mapplic.js')}}"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/rickshaw/rickshaw.min.js')}}"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-metrojs/MetroJs.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/skycons/skycons.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}"
    type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}"
    type="text/javascript"></script>
<script src="{{ URL::asset('new_assets/assets/plugins/datatables-responsive/js/datatables.responsive.js')}}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ URL::asset('new_assets/pages/js/pages.js')}}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ URL::asset('new_assets/assets/js/scripts.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<!-- <script src="assets/js/dashboard.js" type="text/javascript"></script> -->
<script src="{{ URL::asset('new_assets/assets/js/scripts.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function (e) {
        $("#tableEnvelope").dataTable({
            "sDom": "<t><'row'<p i>>",
            "paging": false,
            "info": false,
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        })

        $("#tableStore").dataTable({
            "sDom": "<t><'row'<p i>>",
            "paging": false,
            "info": false,
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        })
        $("#tableStore1").dataTable({
            "sDom": "<t><'row'<p i>>",
            "paging": false,
            "info": false,
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        })
    })
</script>
<!-- END PAGE LEVEL JS -->
@endsection 